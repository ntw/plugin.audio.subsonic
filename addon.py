import libsonic
import os
import time
from tempfile import NamedTemporaryFile
from kodiswift import Plugin

plugin = Plugin()
plugin_path = 'plugin://plugin.audio.subsonic'


def get_connection():

    server_url = plugin.get_setting('server')
    if server_url == "https://example.com":
        plugin.notify('You need to configure your connection!')
    username = plugin.get_setting('username')
    password = plugin.get_setting('password')
    port = plugin.get_setting('port', int)
    conn = libsonic.Connection(server_url,
                               username,
                               password,
                               port=port)
    return conn


@plugin.route('/')
def index():

    items = []

    routes = [('Artists', 'artists'),
              ('Albums', 'albums'),
              ('Settings', 'settings')]

    for route in routes:
        items.append({
            'label': route[0],
            'path': plugin_path + '/' + route[1],
            'is_playable': False
            })
    return items


@plugin.route('/albums')
def albums():

    types = [
            ('Random', 'random'),
            ('Newest', 'newest'),
            ('Highes Rated', 'highest'),
            ('Frequently listened', 'frequent'),
            ('Recently listened', 'recent')
            ]
    items = []
    for t in types:
        items.append({
            'label': t[0],
            'path': plugin_path + '/album_list/' + t[1],
            'is_playable': False
        })
    return items


@plugin.route('/album_list/<list_type>')
def album_list(list_type):
    conn = get_connection()

    albums = conn.getAlbumList(list_type)['albumList']['album']

    items = []
    for album in albums:
        items.append({
            'label': album['title'],
            'path': plugin_path + '/album/' + album['id'],
            'is_playable': False
        })
    return items


@plugin.route('/artists')
def artists():
    conn = get_connection()
    artists = conn.getIndexes()['indexes']['index']

    items = []
    for artist in artists:
        for name in artist['artist']:
            plugin.log.info(artist)
            items.append({
                'label': name['name'],
                'path': plugin_path + '/artist/' + name['id'],
                'is_playable': False
            })
    return items


@plugin.route('/artist/<artist_id>')
def artist(artist_id):
    conn = get_connection()
    albums = conn.getArtist(artist_id)['artist']['album']

    items = []
    for album in albums:
        items.append({
            'label': album['title'],
            'path': plugin_path + '/album/' + album['id'],
            'is_playable': False
        })
    return items


@plugin.route('/album/<album_id>')
def album(album_id):
    conn = get_connection()
    songs = conn.getAlbum(album_id)['album']['song']

    items = []
    for song in songs:
        items.append({
            'label': song['title'],
            'path': plugin_path + '/play_song/' + song['id'],
            'is_playable': True
        })
    return items


@plugin.route('/settings')
def open_settings():
    plugin.open_settings()


@plugin.route('/play_song/<song_id>')
def play_song(song_id):
    song = song_cache_lookup(song_id)
    item = {
        'label': song['title'],
        'path': song['path'],
        'is_playable': True
    }
    plugin.set_resolved_url(item)


def song_cache_lookup(song_id):
    song_cache = plugin.get_storage('song_cache')
    prune_cache(song_cache)

    if song_id in song_cache and \
            os.path.exists(song_cache[song_id]['path']):
        return song_cache[song_id]
    else:
        conn = get_connection()
        song = conn.getSong(song_id)['song']
        path = plugin.storage_path + song_id + '.mp3'
        stream = conn.stream(song_id, tformat='mp3')
        with NamedTemporaryFile(delete=False) as songfile:
            songfile.write(stream.read())
        path = songfile.name
        plugin.log.info("Writing to : " + path)
        song['path'] = path
        song['time_added'] = time.time()
        song_cache[song_id] = song
        return song


def prune_cache(cache):
    size = cache_size(cache)
    plugin.log.info("Cache size is: %d" % size)
    max_size = plugin.get_setting('cachesize', int) * 1024

    for song in sorted(cache.values(), key=lambda x: x['time_added'])[::-1]:
        if size < max_size:
            break
        plugin.log.info("Removing " + song['title'] + " from cache")
        path = song['path']
        size -= song['size']
        if os.path.exists(path):
            os.remove(path)
        del cache[song['id']]


def cache_size(cache):
    size = 0
    for song in cache.values():
        plugin.log.info(song)
        size += int(song['size'])
    return size


if __name__ == '__main__':
    plugin.run()
